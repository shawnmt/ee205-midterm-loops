///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

//struct ThreeArraysStruct threeArrays;

int main() {

   int i, j, k;
   long sum1, sum2, sum3;
   sum1 = 0;
   sum2 = 0;
   sum3 = 0;
   i = 0;
   j = 0;
   k = 0;


   // Sum array1[] with a for() loop and print the result
   for (int i=0; i < 100; i++){
      sum1 += threeArrays.array1[i];
   }

   // Sum array2[] with a while() { } loop and print the result
   while (j < 100){
      sum2 += threeArrays.array2[j];
      j++;
   }

   // Sum array3[] with a do { } while() loop and print the result
   do {
      sum3 += threeArrays.array3[k];
      k++;}
   while (k<100);

   printf("%d\n", sum1);
   printf("%d\n", sum2);
   printf("%d\n", sum3);

	return 0;
}

